/** \file
 * Simply drive an rgb value to a neopixel strip
 * To launch it:
 * > sudo neopixel_set_rgb 12 ws281x.bin
 * where 12 is the number of LED in the strip and ws281x.bin is the binary for the PRU.
 * Once launched, just enter the index and the rgb value for LED. For example:
 * > 2
 * > 255
 * > 0
 * > 100
 * then enter 'd' to draw
 * > d
 * optionally
 * > b
 * to change the buffer. Typically this feature is used only if all the leds of the strip are configured otherwise those not configured will be in unknown color 
 * will turn the third LED (the index starts with 0) with R=255,G=0,B=100
 * If the program is launched with d as option, it will print out some more debug information
 */
#include <cstdio>
#include <cstdlib>
#include <cstdint>
#include <cstring>
#include <ctime>
#include <cinttypes>
#include <cerrno>
#include <iostream>
#include "../pixel.hpp"
#include <sstream>

#define AUTOCLEAR
/*
int32_t getnumber(uint32_t* number){
    std::string line;
    while (std::getline(std::cin, line))
    {
        std::stringstream ss(line);
        if (ss >> *number)
        {
            if (ss.eof())
            {
                return 0;
            }
        }
        std::cout << "Error! Not a number." << std::endl;
    }
    return -1;
    //std::cout << "Finally: " << d << std::endl;
}*/

int32_t getnumber(uint32_t *number){
    if(std::cin >> *number){
        return 0;
    }else{
        if(std::cin.fail()){
            std::cin.clear();
        }
        return -1;
    }
}

int32_t getchar(char *c){
    if(std::cin >> *c){
        return 0;
    }else{
        if(std::cin.fail()){
            std::cout << "clearing" << std::endl;
            std::cin.clear();
        }
        return -1;
    }
}


int main(int argc, char* argv[]) {
  static uint32_t num_pixels;
  static PixelBone_Pixel *strip;
  static uint32_t pix=0; //holds the pixel to drive
  static uint32_t debug=0;
  //printf("argc: %d\n",argc);
  //printf("The arguments supplied are:\n");
  //for(int i = 0; i < argc; i++){
  //    printf("%s\t", argv[i]);
  //}
  if( argc >= 3 ){
    if(argc >= 4){
        if(*argv[3]=='d'){
            debug=1;
        }else{
            std::cout << "Unknown argument: " << argv[3] << std::endl;
        }
    }
    sscanf(argv[1],"%d",&num_pixels);
    strip = new PixelBone_Pixel(num_pixels,argv[2]);
    //printf("Strip object of lenght %d created!",num_pixels);
    if(debug){
        std::cout << "Strip object of lenght " << num_pixels << " created!" << std::endl;
    }
#ifdef AUTOCLEAR
    strip->clear();
    strip->show();
#endif
    while(pix<num_pixels){
      static uint32_t rgb[3]={0,0,0};
      static std::string line;
      static uint32_t done=false,i=0;
      //scanf("%u %u %u %u",&pix,&r,&g,&b);
      i=0;
      while(!done){
        if(i==0){//acquiring pix
            if(getnumber(&pix)==0){
                if(pix>num_pixels){
                    done=true;
                }
            }else{
                done=true;
            }
        }else{  //acquiring rgb
            if(getnumber(&rgb[i-1])==0 && rgb[i-1]<=255){
            }else{
                //std::cout << "Invalid selection, it must be a number between 0 and 255, retry...";
                //i--;
                //std::cin.ignore();
                done=true;
            }
        }

        if(i==3){
            if(debug) std::cout << "Pixel idx: " << pix << std::endl;
            if(debug) std::cout << "Red: " << rgb[0] << std::endl;
            if(debug) std::cout << "Green: " << rgb[1] << std::endl;
            if(debug) std::cout << "Blue: " << rgb[2] << std::endl;

            strip->setPixelColor(pix, rgb[0], rgb[1], rgb[2]);
        }

        i++;
        if(i>3) i=0;
      }
      done=false;
      char c;
      if(pix<num_pixels){
        if(getchar(&c)==0 && c=='d'){
            if(debug){
                std::cout << "Showing..." << std::endl;
            }
            const uint32_t response = strip->wait();
            strip->show();
            std::cout << "Done!" << std::endl;
        }else if(c=='b'){
            if(debug){
                std::cout << "Switch to next buffer." << std::endl;
            }
            strip->moveToNextBuffer(); //if only few leds are changed in between successive calls to ->show(), it is better to not call this function 
        }else{
            if(debug){
                std::cout << "Unknown character..." << std::endl;
            }
            std::cin.ignore();
        }
      }
    }
    printf("closing...\n");
    delete strip;
  }
  else
  {
    printf("argument list is invalid.\nThe expected sintax is: neopixel_set_rgb <strip_length> <pru_binary_location>\n");
  }
  return EXIT_SUCCESS;
}
